// path: /api/todo/:busqueda


const { Router } = require('express');
const { getBusqueda, getBusquedaColeccion } = require('../controllers/busquedas-controlles');
const { validarJWT } = require('../middlewares/validar-jwt');

const router = Router();

router.get('/:busqueda', validarJWT, getBusqueda);
router.get('/coleccion/:tabla/:busqueda', validarJWT, getBusquedaColeccion);





module.exports = router;