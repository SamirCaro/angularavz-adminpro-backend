// path: /api/login

const { Router } = require('express');
const { loginUsuarios, loginUsuariosGoogle, renewToken } = require('../controllers/auth-controlles');
const { validarCampos } = require('../middlewares/validar-campos');
const { check } = require('express-validator');
const { validarJWT } = require('../middlewares/validar-jwt');


const router = Router();

router.post('/', [
        check('password', 'El password es obligatorio').not().isEmpty(),
        check('email', 'El email es obligatorio').isEmail(),
        validarCampos
    ],
    loginUsuarios);

router.post('/google', [
        check('token', 'El token de google es obligatorio').not().isEmpty(),
        validarCampos
    ],
    loginUsuariosGoogle);

router.get('/renew', [
        validarJWT
    ],
    renewToken);

module.exports = router;