// / path: /api/upload/


const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos')
const expressFileUpload = require('express-fileupload');
const { uploadFile, retornarImagen } = require('../controllers/upload-controlles');
const { validarJWT } = require('../middlewares/validar-jwt');

const router = Router();

router.use(expressFileUpload());

router.put('/:tabla/:id', validarJWT, uploadFile);
router.get('/:tabla/:imagen', retornarImagen);






module.exports = router;