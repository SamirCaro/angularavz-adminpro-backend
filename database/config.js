const mongoose = require('mongoose');
require('dotenv').config();

const dbConection = async() => {

    try {
        mongoose.connect(process.env.DB_CNN)
        console.log('DB conectada');
    } catch (error) {
        console.log(error);
        throw new Error('Error en la conexion con la DB ver logs')
    }

}

module.exports = {
    dbConection
}