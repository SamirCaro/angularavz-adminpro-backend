const path = require('path'); //para retornar path de la imagen para poderla ver
const fs = require('fs'); //Permite validar si existe un archivo en la ruta especificada
const { response } = require("express");
const { v4: uuidv4 } = require('uuid');
const { actualizarImagen } = require("../helpers/actualizar-imagen");

const uploadFile = async(req, resp = response) => {
    const tabla = req.params.tabla;
    const id = req.params.id;
    const tablasValidas = ['usuarios', 'hospitales', 'medicos']
    if (!tablasValidas.includes(tabla)) {
        return resp.status(400).json({
            ok: false,
            msg: 'Se debe subir el archivo a la tabla usuarios o medicos o hospitales'
        })
    }
    //Validar que exista una imagen
    if (!req.files || Object.keys(req.files).length === 0) {
        return resp.status(400).json({
            ok: false,
            msg: 'No hay ningun archivo'
        })
    }
    //captura de la imagen
    const file = req.files.imagen;
    // console.log(file);

    //pasos para tomar la extension del archvio
    const nombreSegmentado = file.name.split('.'); //wolverine.1.2.jpg
    const extensionArchivo = nombreSegmentado[nombreSegmentado.length - 1];

    //Pasos para validar la extension 
    const extensionesValidas = ['jpg', 'gif', 'png', 'jpeg'];
    if (!extensionesValidas.includes(extensionArchivo)) {
        return resp.status(400).json({
            ok: false,
            msg: 'La extension del archivo no es valida'
        })
    }

    //Crear un nombre unico para la imagen
    const nombreArchivo = `${uuidv4()}.${extensionArchivo}`;

    //Crear Path para guardar la imagen
    const path = `./uploads/${tabla}/${nombreArchivo}`;

    // Mover la imagen al path especifico segun tabla
    file.mv(path, (err) => {
        if (err) {
            console.log(err);
            return res.status(500).json({
                ok: false,
                msg: 'Error al mover la imagen'
            })
        }

        //Actualizar imagen en la base de datos
        actualizarImagen(tabla, id, nombreArchivo);

        resp.json({
            ok: true,
            msg: 'Archivo subido',
            nombreArchivo
        })
    });

}

const retornarImagen = (req, resp = response) => {
    const tabla = req.params.tabla;
    const imagen = req.params.imagen;
    const pathImg = path.join(__dirname, `../uploads/${tabla}/${imagen}`);
    if (fs.existsSync(pathImg)) {
        resp.sendFile(pathImg);
    } else {
        const pathImg = path.join(__dirname, `../uploads/no-img.jpg`);
        resp.sendFile(pathImg);
    }



}

module.exports = {
    uploadFile,
    retornarImagen
}