const { response } = require("express");
const Usuario = require('../models/usuario.model');
const bcrypt = require('bcryptjs');
const { generarJWT } = require("../helpers/jwt");
const { googleVerify } = require("../helpers/google-verify");

const loginUsuarios = async(req, resp = response) => {
    const { email, password } = req.body;
    try {
        const usuarioDB = await Usuario.findOne({ email });
        if (!usuarioDB) {
            return resp.status(400).json({
                ok: false,
                msg: 'No existe un usuario con este email'
            })
        }

        const validPassword = bcrypt.compareSync(password, usuarioDB.password);

        if (!validPassword) {
            return resp.status(400).json({
                ok: false,
                msg: 'Contraseña no valida'
            })
        }

        const token = await generarJWT(usuarioDB.id);

        resp.json({
            ok: true,
            token
        })

    } catch (error) {
        console.log(error);
        resp.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        })
    }
}

const loginUsuariosGoogle = async(req, resp = response) => {

    try {
        const { email, name, picture } = await googleVerify(req.body.token);
        const usuarioDB = await Usuario.findOne({ email });
        let usuario;
        if (!usuarioDB) {
            usuario = new Usuario({
                nombre: name,
                email,
                password: '@@@',
                img: picture,
                google: true
            })
        } else {
            usuario = usuarioDB;
            usuario.google = true
        }

        await usuario.save();

        const token = await generarJWT(usuario.id);

        resp.json({
            ok: true,
            email,
            name,
            picture,
            token
        })

    } catch (error) {

        resp.status(400).json({
            ok: false,
            msg: 'token de google no valido'
        })
    }


}

const renewToken = async(req, resp = response) => {
    const uidUsuario = req.uid;

    const token = await generarJWT(uidUsuario);

    resp.json({
        ok: true,
        token
    })
}

module.exports = {
    loginUsuarios,
    loginUsuariosGoogle,
    renewToken
}