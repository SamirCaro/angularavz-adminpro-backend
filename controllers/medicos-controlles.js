const { response } = require("express");
const Medico = require('../models/medico.model');




const getMedicos = async(req, resp = response) => {

    const medicosDB = await Medico.find()
        .populate('usuario', 'nombre img')
        .populate('hospital', 'nombre img')

    resp.json({
        ok: true,
        medicos: medicosDB
    })
}

const crearMedico = async(req, resp = response) => {
    const uidUsuario = req.uid;
    const medico = new Medico({
        usuario: uidUsuario,
        ...req.body
    })
    try {
        const medicoDB = await medico.save();
        return resp.json({
            ok: true,
            medico: medicoDB
        })

    } catch (error) {
        console.log(error);
        return resp.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        })
    }




}

const actualizarMedico = async(req, resp = response) => {

    try {
        const uidMedico = req.params.id;
        const uidUsuario = req.uid;
        const medicoDB = await Medico.findById(uidMedico);
        if (!medicoDB) {
            return resp.status(404).json({
                ok: false,
                msg: 'No existe un medico con ese id',
            })
        }

        const NuevosDatosMedico = {
            ...req.body,
            usuario: uidUsuario
        }

        const medicoActualizado = await Medico.findByIdAndUpdate(uidMedico, NuevosDatosMedico, { new: true });
        resp.json({
            ok: true,
            msg: 'medico actualizado',
            medico: medicoActualizado
        })

    } catch (error) {
        console.log(error);
        resp.status(500).json({
            ok: true,
            msg: 'Error inesperado... revisar logs'
        })
    }


}

const borrarMedico = async(req, resp = response) => {
    try {
        const uidMedico = req.params.id;
        const medicoDB = await Medico.findById(uidMedico);
        if (!medicoDB) {
            return resp.status(404).json({
                ok: false,
                msg: 'No existe un medico con ese id',
            })
        }

        await Medico.findByIdAndDelete(uidMedico);
        resp.json({
            ok: true,
            msg: 'medico borrado',
            medico: medicoDB
        })

    } catch (error) {
        console.log(error);
        resp.status(500).json({
            ok: true,
            msg: 'Error inesperado... revisar logs'
        })
    }
}


module.exports = {
    getMedicos,
    crearMedico,
    actualizarMedico,
    borrarMedico
}