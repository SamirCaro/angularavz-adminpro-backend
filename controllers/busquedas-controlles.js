const { response } = require("express");
const Usuario = require('../models/usuario.model');
const Hospital = require('../models/hospital.model');
const Medico = require('../models/medico.model');




const getBusqueda = async(req, resp = response) => {
    const terminoBusqueda = req.params.busqueda;
    const regex = new RegExp(terminoBusqueda, 'i'); // Variable regular cambio para busqueda global insensitive
    const [resulUsuarios, resulHospitales, resulMedicos] = await Promise.all([
        Usuario.find({ nombre: regex }),
        Hospital.find({ nombre: regex }),
        Medico.find({ nombre: regex })
    ])

    resp.json({
        ok: true,
        resulUsuarios,
        resulHospitales,
        resulMedicos
    })
}

const getBusquedaColeccion = async(req, resp = response) => {
    const tabla = req.params.tabla;
    const terminoBusqueda = req.params.busqueda;
    const regex = new RegExp(terminoBusqueda, 'i');
    let resulBusqueda = [];

    switch (tabla) {
        case 'usuarios':
            resulBusqueda = await Usuario.find({ nombre: regex });
            break;
        case 'hospitales':
            resulBusqueda = await Hospital.find({ nombre: regex })
                .populate('usuario', 'nombre img');

            break;
        case 'medicos':
            resulBusqueda = await Medico.find({ nombre: regex })
                .populate('usuario', 'nombre img')
                .populate('hospital', 'nombre img');
            break;

        default:
            return resp.status(400).json({
                ok: false,
                msd: 'La tabla de busqueda tiene que ser de usuarios o medicos o hospitales'
            })

    }
    resp.json({
        ok: true,
        resultadoBusqueda: resulBusqueda
    })
}

module.exports = {
    getBusqueda,
    getBusquedaColeccion
}