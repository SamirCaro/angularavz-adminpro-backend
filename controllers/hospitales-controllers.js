const { response } = require("express");
const Hospital = require('../models/hospital.model');



const getHospitales = async(req, resp = response) => {
    const hospitalesDB = await Hospital.find()
        .populate('usuario', 'nombre img')

    resp.json({
        ok: true,
        hospitales: hospitalesDB
    })
}

const crearHospital = async(req, resp = response) => {
    const uidUsuario = req.uid;
    const hospital = new Hospital({
        usuario: uidUsuario,
        ...req.body
    });

    try {

        const hospitalDB = await hospital.save();

        resp.json({
            ok: true,
            hospital: hospitalDB
        })

    } catch (error) {
        console.log(error);
        resp.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        })
    }
}

const actualizarHospital = async(req, resp = response) => {

    try {
        const uidHospital = req.params.id;
        const uidUsuario = req.uid;
        const hospitalDB = await Hospital.findById(uidHospital);
        if (!hospitalDB) {
            return resp.status(404).json({
                ok: false,
                msg: 'No existe un hospital con ese id',
            })
        }

        const NuevosDatosHospital = {
            ...req.body,
            usuario: uidUsuario
        }

        const hospitalActualizado = await Hospital.findByIdAndUpdate(uidHospital, NuevosDatosHospital, { new: true });
        resp.json({
            ok: true,
            msg: 'hospital actualizado',
            hospital: hospitalActualizado
        })

    } catch (error) {
        console.log(error);
        resp.status(500).json({
            ok: true,
            msg: 'Error inesperado... revisar logs'
        })
    }



}

const borrarHospital = async(req, resp = response) => {
    try {
        const uidHospital = req.params.id;

        const hospitalDB = await Hospital.findById(uidHospital);
        if (!hospitalDB) {
            return resp.status(404).json({
                ok: false,
                msg: 'No existe un hospital con ese id',
            })
        }



        await Hospital.findByIdAndDelete(uidHospital);

        resp.json({
            ok: true,
            msg: 'hospital Borrado',
            hospital: hospitalDB
        })

    } catch (error) {
        console.log(error);
        resp.status(500).json({
            ok: true,
            msg: 'Error inesperado... revisar logs'
        })
    }
}


module.exports = {
    getHospitales,
    crearHospital,
    actualizarHospital,
    borrarHospital
}