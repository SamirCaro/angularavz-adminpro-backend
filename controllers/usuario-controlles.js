const { response } = require('express');
const Usuario = require('../models/usuario.model');
const bcrypt = require('bcryptjs');
const { generarJWT } = require("../helpers/jwt");

const getUsuarios = async(req, resp) => {
    const desde = Number(req.query.desde) || 0;

    const [usuarios, total] = await Promise.all([
        Usuario.find({}, 'nombre email role google img')
        .skip(desde)
        .limit(5),
        Usuario.countDocuments()
    ])


    resp.json({
        ok: true,
        usuarios: usuarios,
        total
    })
}


const crearUsuario = async(req, resp) => {
    const { email, password, nombre } = req.body;
    try {
        const existeEmail = await Usuario.findOne({ email })
        if (existeEmail) {
            return resp.status(400).json({
                ok: false,
                msg: 'ya existe un usuario con este email'
            })
        }
        const usuario = new Usuario(req.body);

        //Encriptar contraseña del usuario
        const salt = bcrypt.genSaltSync();
        usuario.password = bcrypt.hashSync(password, salt);

        //Guardar usuario en la base de datos
        await usuario.save();

        //generar JWT
        const token = await generarJWT(usuario.uid);

        resp.json({
            ok: true,
            usuario: usuario,
            token
        })

    } catch (error) {
        console.log(error);
        resp.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        })
    }
}

const actualizarUsuario = async(req, resp = response) => {
    const uid = req.params.id;
    const { password, google, email, ...campos } = req.body;
    try {
        const usarioDB = await Usuario.findById(uid);
        if (!usarioDB) {
            return resp.status(400).json({
                ok: false,
                msg: 'No existe un usuario con este Id'
            })
        }
        if (usarioDB.email !== email) {
            const existeEmail = await Usuario.findOne({ email });
            if (existeEmail) {
                return resp.status(400).json({
                    ok: false,
                    msg: 'Ya existe un usuario con este Email'
                })
            }
        }
        campos.email = email;
        const usuarioActualizado = await Usuario.findByIdAndUpdate(uid, campos, { new: true })

        resp.json({
            ok: true,
            usuario: usuarioActualizado

        })

    } catch (error) {
        console.log(error);
        resp.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        })
    }
}

const borrarUsuario = async(req, resp = response) => {
    const uid = req.params.id
    try {
        const usarioDB = await Usuario.findById(uid);
        if (!usarioDB) {
            return resp.status(400).json({
                ok: false,
                msg: 'No existe un usuario con este Id'
            })
        }
        await Usuario.findByIdAndDelete(uid)
        resp.json({
            ok: true,
            msg: 'usuario eliminado'
        })

    } catch (error) {
        console.log(error);
        resp.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        })
    }
}

module.exports = {
    getUsuarios,
    crearUsuario,
    actualizarUsuario,
    borrarUsuario
}