require('dotenv').config(); //Importar paquete para leer variables de entorno
const express = require('express');

const cors = require('cors'); //Importar paquete para habilitar las peticiones de  manera global
const { dbConection } = require('./database/config')

//crear servidor express
const app = express();

//habilitar uso de cors
app.use(cors());

//lectura y parseo del body
app.use(express.json());

app.use(express.static('public'));

//Conexion a la base de datos
dbConection();


//User Mongo: mean1_user
//Password:sH5WHpZvNBLiwsMb

//creacion de rutas para peticiones
app.use('/api/usuarios', require('./routes/usuario-routes')); //CRUD USUARIOS
app.use('/api/login', require('./routes/auth-routes')); //Login
app.use('/api/hospitales', require('./routes/hospitales-routes')); //CRUD HOSPITALES
app.use('/api/medicos', require('./routes/medicos-routes')); //CRUD MEDICOS
app.use('/api/todo', require('./routes/busquedas-routes')); //Busquedas
app.use('/api/upload', require('./routes/upload-routes')); //Subir archivos


//configuracion para escuchar peticiones
app.listen(process.env.PORT, () => {
    console.log('servidor corriendo puerto ' + process.env.PORT);
})