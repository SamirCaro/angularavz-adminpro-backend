const fs = require('fs'); //Permite validar si existe un archivo en la ruta especificada
const Hospital = require('../models/hospital.model');
const Medico = require('../models/medico.model');
const Usuario = require('../models/usuario.model');

const borrarImagen = (path) => {
    if (fs.existsSync(path)) {
        fs.unlinkSync(path)
    }
}

const actualizarImagen = async(tabla, id, nombreArchivo) => {
    let pathViejo = '';
    switch (tabla) {
        case 'medicos':
            const medico = await Medico.findById(id);
            if (!medico) {
                return false;
            }
            pathViejo = `./uploads/${tabla}/${medico.img}`;
            borrarImagen(pathViejo);
            medico.img = nombreArchivo;
            medico.save();
            return true
            break;
        case 'hospitales':
            const hospital = await Hospital.findById(id);
            if (!hospital) {
                return false;
            }
            pathViejo = `./uploads/${tabla}/${hospital.img}`;
            borrarImagen(pathViejo);
            hospital.img = nombreArchivo;
            hospital.save();
            return true
            break;
        case 'usuarios':
            const usuario = await Usuario.findById(id);
            if (!usuario) {
                return false;
            }
            pathViejo = `./uploads/${tabla}/${usuario.img}`;
            borrarImagen(pathViejo);
            usuario.img = nombreArchivo;
            usuario.save();
            return true
            break;
        default:

            break;
    }
}

module.exports = {
    actualizarImagen
}