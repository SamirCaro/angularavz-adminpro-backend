const { response } = require("express");
const JWT = require('jsonwebtoken');
const validarJWT = (req, resp = response, next) => {
    //leer token
    const token = req.header('x-token');
    console.log(token);
    if (!token) {
        return resp.status(400).json({
            ok: false,
            msg: 'No se envio un token'
        })
    }
    try {
        const validacionToken = JWT.verify(token, process.env.JWT_SECRET);
        req.uid = validacionToken.uid;
    } catch (error) {
        return resp.status(401).json({
            ok: false,
            msg: 'Token invalido'
        })
    }
    next();
}
module.exports = {
    validarJWT
}